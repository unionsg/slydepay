<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;

class UsedFunctions extends Model
{
    //
    /**
     * This function helps to post json request to a url
     * 
     * Accepts URL and JSON body
     */
    public function postJson($path,$body){
        Log::debug($body);
        Log::debug(json_encode($body));
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $path,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($body),
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
