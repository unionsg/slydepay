<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{   
    var $emailOrMobileNumber;
    var $merchantKey;
    var $amount;
    var $orderCode;
    var $description;
    var $sendInvoice;
    var $payOption;
    var $customerName;
    var $customerMobileNumber;
    var $payToken;
    var $transactionId;

    public function __construct($emailOrMobileNumber,$merchantKey) {
        $this->emailOrMobileNumber = $emailOrMobileNumber;
        $this->merchantKey = $merchantKey;
    }

    public function getEmailOrMobileNumber(){
        return $this->emailOrMobileNumber;
    }

    public function setEmailOrMobileNumber($emailOrMobileNumber) {
        $this->emailOrMobileNumber = $emailOrMobileNumber;
    }

    public function getMerchantKey()
    {
        return $this->merchantKey;
    }

    public function setMerchantKey($merchantKey)
    {
        $this->merchantKey = $merchantKey;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getOrderCode()
    {
        return $this->orderCode;
    }

    public function setOrderCode($orderCode)
    {
        $this->orderCode = $orderCode;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getSendInvoice()
    {
        return $this->sendInvoice;
    }

    public function setSendInvoice($sendInvoice)
    {
        $this->sendInvoice = $sendInvoice;
    }

    public function getPayOption()
    {
        return $this->payOption;
    }

    public function setPayOption($payOption)
    {
        $this->payOption = $payOption;
    }

    public function getCustomerName()
    {
        return $this->customerName;
    }

    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    public function getCustomerMobileNumber()
    {
        return $this->customerMobileNumber;
    }

    public function setCustomerMobileNumber($customerMobileNumber)
    {
        $this->customerMobileNumber = $customerMobileNumber;
    }

    public function getPayToken()
    {
        return $this->payToken;
    }

    public function setPayToken($payToken)
    {
        $this->payToken = $payToken;
    }

    public function getTransactionId()
    {
        return $this->transactionId;
    }

    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

   
    public function initiateArray() 
    {
        return array (
        'emailOrMobileNumber' => $this->emailOrMobileNumber,
        'merchantKey' => $this->merchantKey,
        'amount' => $this->amount,
        'orderCode' => $this->orderCode,
        'description' => $this->description,
        'sendInvoice' => true,
        'payOption' => $this->payOption,
        'customerName' => $this->customerName,
        'customerMobileNumber' => $this->customerMobileNumber
        )  ;
    }

    public function checkStatus()
    {
        return array (
            'emailOrMobileNumber' => $this->emailOrMobileNumber,
            'merchantKey' => $this->merchantKey,
            'orderCode' => $this->orderCode,
            'payToken' => $this->payToken,
            'confirmTransaction' => true
        );
    }

    public function confrimPayment()
    {
        return array(
            'emailOrMobileNumber' => $this->emailOrMobileNumber,
            'merchantKey' => $this->merchantKey,
            'orderCode' => $this->orderCode,
            'payToken' => $this->payToken,
            'transactionId' => $this->transactionId
        );
    }

}
