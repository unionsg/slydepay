<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsedFunctions;
use App\Invoice;
use Log;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return json_decode($request->getContent(), true);
        $merchantId = env('merchantId', 'ash11nb@yahoo.com');
        $merchantKey = env('merchantKey', '1548273577453');
        $invoice = new Invoice($merchantId,$merchantKey);
        $invoice->setOrderCode($request->transactionReference);
        $invoice->setAmount($request->amount);
        $invoice->setDescription($request->narration);
        $invoice->setPayOption($request->network);
        $invoice->setCustomerName($request->customerName);
        $invoice->setCustomerMobileNumber($request->mobileNumber);

        $req = $invoice->initiateArray();
     

        $func = new UsedFunctions();
        $response = $func->postJson(env("SLYDE_PAY"). "invoice/create",$req);
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $merchantId = env('MERCHANT_ID');
        $merchantKey = env('MERCHANT_KEY');
        $invoice = new Invoice($merchantId, $merchantKey);
        $invoice->setOrderCode($request->transactionReference);
        $invoice->setPayToken($request->payToken);

        $req = $invoice->checkStatus();


        $func = new UsedFunctions();
        $response = $func->postJson(env('SLYDE_PAY'). "invoice/checkstatus", $req);
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $merchantId = env('MERCHANT_ID');
        $merchantKey = env('MERCHANT_KEY');
        $invoice = new Invoice($merchantId, $merchantKey);
        $invoice->setOrderCode($request->transactionReference);
        $invoice->setPayToken($request->payToken);
        $invoice->setTransactionId($request->callBackId);

        $req = $invoice->confrimPayment();


        $func = new UsedFunctions();
        $response = $func->postJson(env('SLYDE_PAY') . "transaction/confirm", $req);
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
